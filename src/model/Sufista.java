package model;

public class Sufista extends Atleta {
	
	private String campeao;
	private String categoria;
	private String bateria;
	
	public Sufista(String nome) {
		super(nome);
	}

	public String getCampeao() {
		return campeao;
		
	}

	public void setCampeao(String campeao) {
		this.campeao = campeao;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getBateria() {
		return bateria;
	}

	public void setBateria(String bateria) {
		this.bateria = bateria;
	}
	
	

}
